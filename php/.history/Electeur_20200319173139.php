<?php
require 'Personne';
class Electeur extends Personne
{
    private $bureauvote ;
    private $vote ;

    public function __construct($bureauvote,$nom,$prenom)
    {
        parent::__construct($nom,$prenom);
        $this->vote = false ;
        $this->bureauvote = $bureauvote ;
    }
    public function voter()
    {
        $this->vote = true ;
    }

}